package ca.csf.simplefx;

import javafx.stage.Stage;

public abstract class Application extends javafx.application.Application {
    private Stage primaryStage;

    protected void onCreate() {
        //Empty by default
    }

    protected void onDestroy() {
        //Empty by default
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    @Override
    @Deprecated
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        onCreate();
    }

    @Override
    @Deprecated
    public void stop() throws Exception {
        onDestroy();
    }
}
