package ca.csf.simplefx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

public abstract class Window {
    private final Application application;
    private final Stage stage;
    private final Scene scene;

    public Window(Application application, Parent root) {
        this(application, root, null);
    }

    public Window(Application application, Parent root, String cssUrl) {
        this.application = application;
        this.stage = createStage(application);
        this.scene = createScene(root, cssUrl);

        init();
    }

    public Window(Application application, String fxmlUrl) {
        this(application, fxmlUrl, null);
    }

    public Window(Application application, String fxmlUrl, String cssUrl) {
        this.application = application;
        this.stage = createStage(application);
        this.scene = createScene(fxmlUrl, cssUrl);

        init();
    }

    private void init() {
        //Stages and scenes have a one to one relationship in SimpleFx
        stage.setScene(scene);

        //onShowing and onHiding event are bounded to SimpleFx onResume and onPause events.
        stage.setOnShowing(event -> onResume());
        stage.setOnHiding(event -> onPause());

        onCreate();
    }

    private Stage createStage(Application application) {
        Stage primaryStage = application.getPrimaryStage();
        if (primaryStage.getScene() != null) return new Stage();
        else return primaryStage;
    }

    private Scene createScene(Parent root, String cssUrl) {
        Scene scene = new Scene(root);
        if (cssUrl != null) scene.getStylesheets().add(getClass().getResource(cssUrl).toExternalForm());
        return scene;
    }

    private Scene createScene(String fxmlUrl, String cssUrl) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(fxmlUrl));
            loader.setControllerFactory(param -> this); //By using a controller factory, the user can keep the "fx:controller" attribute.
            Parent load = loader.load();
            return createScene(load, cssUrl);
        } catch (IllegalStateException e) {
            throw new RuntimeException("Unable to create Window. The Fxml file path is probably not valid.", e);
        } catch (IOException e) {
            throw new RuntimeException("Unable to create Window. The Fxml file content is probably not valid.", e);
        }
    }

    protected void onCreate() {
        //Empty by default
    }

    protected void onResume() {
        //Empty by default
    }

    protected void onPause() {
        //Empty by default
    }

    public Application getApplication() {
        return application;
    }

    public Stage getStage() {
        return stage;
    }

    public Scene getScene() {
        return scene;
    }

    public String getTitle() {
        return stage.getTitle();
    }

    public void setTitle(String title) {
        stage.setTitle(title);
    }

    public String getIcon() {
        List<Image> icons = stage.getIcons();
        if (icons.isEmpty()) return null;
        else return icons.get(0).getUrl();
    }

    public void setIcon(String iconUrl) {
        List<Image> icons = stage.getIcons();
        icons.clear();
        icons.add(new Image(iconUrl));
    }

    public boolean isResizable() {
        return stage.isResizable();
    }

    public void setResizable(boolean resizable) {
        stage.setResizable(resizable);
    }

    @SuppressWarnings("unchecked")
    public <T extends Parent> T getRoot() {
        return (T) scene.getRoot();
    }

    public void setRoot(Parent root) {
        scene.setRoot(root);
    }

    public void show() {
        stage.show();
    }

    public void showAndWait() {
        stage.showAndWait();
    }
}
